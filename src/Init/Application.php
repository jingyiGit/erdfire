<?php

namespace Jy2dfire\Init;

use Jy2dfire\Kernel\Response;
use Jy2dfire\Erdfire\Erdfire;
use Jy2dfire\BasicService\BaseConfig;

/**
 * Class Application.
 */
class Application extends Erdfire
{
  use Response;
  use BaseConfig;
  
  public function __construct(array $config = [])
  {
    parent::__construct($this->initConfig($config));
  }
}
