<?php
// 商品
// https://open.2dfire.com/page/file.html#/docs_api_rights

namespace Jy2dfire\Erdfire;

use Jy2dfire\Kernel\Http;

trait Good
{
  /**
   * 取商品列表
   *
   * @param $params
   * @return mixed
   */
  public function getGoodList($params)
  {
    if (!isset($params['pageIndex'])) {
      $params['pageIndex'] = 1;
    }
    if (!isset($params['pageSize'])) {
      $params['pageSize'] = 50;
    }
    if (!isset($params['range'])) {
      $params['range'] = 3;
    }
    if (!isset($params['type'])) {
      $params['type'] = 3;
    }
    $params         = $this->handleGlobalParam('com.dfire.open.item.menu.query', $params);
    $params['sign'] = $this->getSign($params);
    $res            = Http::httpPost($this->domainUrl, $params);
    if ((isset($res['code']) && $res['code'] == 0) || !isset($res['code'])) {
      $this->setError($res);
      return false;
    }
    return $res;
  }
  
  /**
   * 取商品详情
   *
   * @param string $entityId
   * @param string $orderId
   * @return false|mixed
   */
  public function getGoodDetail($entityId, $orderId)
  {
    $params         = [
      'entityId' => $entityId,
      'objectId' => $orderId,
    ];
    $params         = $this->handleGlobalParam('com.dfire.open.item.menu.detail', $params);
    $params['sign'] = $this->getSign($params);
    $res            = Http::httpPost($this->domainUrl, $params);
    if ((isset($res['code']) && $res['code'] == 0) || !isset($res['code'])) {
      $this->setError($res);
      return false;
    }
    return $res;
  }
  
  /**
   * 取套餐详情
   *
   * @param string $entityId
   * @param string $orderId
   * @return false|mixed
   */
  public function getAssembleDetail($entityId, $orderId)
  {
    $params         = [
      'entityId' => $entityId,
      'objectId' => $orderId,
    ];
    $params         = $this->handleGlobalParam('com.dfire.open.item.assemble.detail', $params);
    $params['sign'] = $this->getSign($params);
    $res            = Http::httpPost($this->domainUrl, $params);
    if ((isset($res['code']) && $res['code'] == 0) || !isset($res['code'])) {
      $this->setError($res);
      return false;
    }
    return $res;
  }
  
  /**
   * 取商品分类列表
   *
   * @param array $params
   * @return mixed
   */
  public function getCategory($params)
  {
    if (!isset($params['pageIndex'])) {
      $params['pageIndex'] = 1;
    }
    if (!isset($params['pageSize'])) {
      $params['pageSize'] = 50;
    }
    $params         = $this->handleGlobalParam('com.dfire.open.item.menu.kind.query', $params);
    $params['sign'] = $this->getSign($params);
    $res            = Http::httpPost($this->domainUrl, $params);
    if ((isset($res['code']) && $res['code'] == 0) || !isset($res['code'])) {
      $this->setError($res);
      return false;
    }
    return $res;
  }
  
  /**
   * 查询菜品数量信息
   * https://open.2dfire.com/page/file.html#/docs_api_interface?interfaceId=7950bd1fc43f4faa8f363743bf1afcbe&interfaceCode=com.dfire.open.menu.balance.query&groupId=38&groupName=%E5%BA%97%E9%93%BA%E8%8F%9C%E5%93%81V2.0
   *
   * @param array $params
   * @return mixed
   */
  public function getBalance($params)
  {
    if (!isset($params['pageIndex'])) {
      $params['pageIndex'] = 1;
    }
    if (!isset($params['pageSize'])) {
      $params['pageSize'] = 50;
    }
    $params         = $this->handleGlobalParam('com.dfire.open.menu.balance.query', $params);
    $params['sign'] = $this->getSign($params);
    $res            = Http::httpPost($this->domainUrl, $params);
    if ((isset($res['code']) && $res['code'] == 0) || !isset($res['code'])) {
      $this->setError($res);
      return false;
    }
    return $res;
  }
}
