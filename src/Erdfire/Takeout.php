<?php
// 外卖
// https://open.2dfire.com/page/file.html#/docs_api_rights?groupId=36&groupName=%E5%A4%96%E5%8D%96%E6%8E%A5%E5%85%A5V2.0

namespace Jy2dfire\Erdfire;

use Jy2dfire\Kernel\Http;

trait Takeout
{
  /**
   * 创建外卖订单
   *
   * @param string $entityId
   * @param array  $orderInfo
   * @param array  $goodList
   * @return mixed
   */
  public function createTakeOutOrder($entityId, $orderInfo, $goodList)
  {
    $orderInfo                 = $this->setDefalutOrderInfo($orderInfo);
    $orderInfo['cartV2BoList'] = $goodList;
    $params                    = [
      'entityId'             => $entityId,
      'thirdpartWMOrderV2Bo' => $orderInfo,
    ];
    
    $params         = $this->handleGlobalParam('com.dfire.open.order.submit.v2', ['req' => json_encode($params, JSON_UNESCAPED_UNICODE)]);
    $params['sign'] = $this->getSign($params);
    $res            = Http::httpPost($this->domainUrl, $params);
    if ((isset($res['code']) && $res['code'] == 0) || !isset($res['code'])) {
      $this->setError($res);
      return false;
    }
    return $res;
  }
  
  /**
   * 取消订单
   *
   * @param $params
   * @return false|mixed
   */
  public function cancelOrder($params)
  {
    if (!isset($params['orderFrom'])) {
      $params['orderFrom'] = 118;
    }
    $params         = ['request' => json_encode($params, JSON_UNESCAPED_UNICODE)];
    $params         = $this->handleGlobalParam('com.dfire.open.takeout.order.cancel', $params);
    $params['sign'] = $this->getSign($params);
    $res            = Http::httpPost($this->domainUrl, $params);
    if ((isset($res['code']) && $res['code'] == 0) || !isset($res['code'])) {
      $this->setError($res);
      return false;
    }
    return $res;
  }
  
  private function setDefalutOrderInfo($orderInfo)
  {
    // 是否支持开发票
    if (!isset($orderInfo['invoiced'])) {
      $orderInfo['invoiced'] = false;
    }
    
    // 是否是预订单（待定）
    if (!isset($orderInfo['book'])) {
      $orderInfo['book'] = false;
    }
    
    // 订单来源，100：百度，101：美团，102：饿了么，118：其它
    if (!isset($orderInfo['orderFrom'])) {
      $orderInfo['orderFrom'] = 118;
    }
    
    // 支付来源，4 :聚合支付 5: 预购支付
    if (!isset($orderInfo['payFrom'])) {
      $orderInfo['payFrom'] = 4;
    }
    
    // 付款方式，1：货到付款 2：在线支付
    if (!isset($orderInfo['payType'])) {
      $orderInfo['payType'] = 2;
    }
    
    // 订单人数
    if (!isset($orderInfo['peopleCount'])) {
      $orderInfo['peopleCount'] = 1;
    }
    
    // 配送时间，下单是可以选择立即送：reserveDate = 0。单位为毫秒。默认为立即送
    if (!isset($orderInfo['reserveDate'])) {
      $orderInfo['reserveDate'] = 0;
    }
    
    // 配送方式，0 表示自配送 1 表示三方配送 2 表示自取
    if (!isset($orderInfo['shippingType'])) {
      $orderInfo['shippingType'] = 0;
    }
    
    // 配送费，单位：分
    if (!isset($orderInfo['outFee'])) {
      $orderInfo['outFee'] = 0;
    }
    return $orderInfo;
  }
}
