<?php

namespace Jy2dfire\Erdfire;

use Jy2dfire\Kernel\Http;

trait Order
{
  /**
   * 根据订单ID，取订单信息
   *
   * @param string       $entityId
   * @param array|string $orderIds
   * @return false|mixed
   */
  public function getOrder($entityId, $orderIds)
  {
    $orderIds       = !is_array($orderIds) ? (array)$orderIds : $orderIds;
    $params         = [
      'entityId' => $entityId,
      'orderIds' => json_encode($orderIds),
    ];
    $params         = $this->handleGlobalParam('com.dfire.open.order.instance.query', $params);
    $params['sign'] = $this->getSign($params);
    $res            = Http::httpPost($this->domainUrl, $params);
    if ((isset($res['code']) && $res['code'] == 0) || !isset($res['code'])) {
      $this->setError($res);
      return false;
    }
    return $res;
  }
  
  /**
   * 取订单列表
   *
   * @param $params
   * @return mixed
   */
  public function getOrders($params)
  {
    if (!isset($params['pageIndex'])) {
      $params['pageIndex'] = 1;
    }
    if (!isset($params['pageSize'])) {
      $params['pageSize'] = 50;
    }
    $params         = $this->handleTime($params);
    $params         = $this->handleGlobalParam('com.dfire.open.shop.order.query', $params);
    $params['sign'] = $this->getSign($params);
    $res            = Http::httpPost($this->domainUrl, $params);
    if ((isset($res['code']) && $res['code'] == 0) || !isset($res['code'])) {
      $this->setError($res);
      return false;
    }
    return $res;
  }
  
  /**
   * 取订单列表，可根据订单状态或订单ID
   *
   * @param array $params
   * @return false|mixed
   */
  public function getOrderByStatus($params)
  {
    if (!isset($params['pageIndex'])) {
      $params['pageIndex'] = 1;
    }
    if (!isset($params['pageSize'])) {
      $params['pageSize'] = 50;
    }
    if (!isset($params['status'])) {
      $statusList = [1, 2, 3, 4, 5];
    } else {
      $statusList = $params['status'];
      unset($params['status']);
    }
    $params         = [
      'req'             => json_encode($this->handleTime($params)),
      'orderStatusList' => json_encode($statusList),
    ];
    $params         = $this->handleGlobalParam('com.dfire.open.order.query', $params);
    $params['sign'] = $this->getSign($params);
    $res            = Http::httpPost($this->domainUrl, $params);
    if ((isset($res['code']) && $res['code'] == 0) || !isset($res['code'])) {
      $this->setError($res);
      return false;
    }
    return $res;
  }
  
  /**
   * 处理时间
   *
   * @param array $params
   * @return mixed
   */
  private function handleTime($params)
  {
    if (!isset($params['currDate']) || ($params['currDate'] > 0 && strlen($params['currDate']) != 8 && strlen($params['currDate']) != 10)) {
      $params['currDate'] = date('Ymd', time());
    } else if ($params['currDate'] < 0) {
      $params['currDate'] = date('Ymd', strtotime($params['currDate'] . ' day', time()));
    } else if (isset($params['currDate']) && strlen($params['currDate']) == 10) {
      $params['currDate'] = date('Ymd', $params['currDate']);
    }
    return $params;
  }
}
