<?php
// 店铺
// https://open.2dfire.com/page/file.html#/docs_api_rights?groupId=29

namespace Jy2dfire\Erdfire;

use Jy2dfire\Kernel\Http;

trait Shop
{
  /**
   * 取店铺信息
   *
   * @param $params
   * @return false|mixed
   */
  public function getShopInfo($params)
  {
    $params['appKey'] = $this->config['key'];
    $params           = $this->handleGlobalParam('com.dfire.open.shop.condition.query', ['entityAuthShopBindQuery' => json_encode($params, JSON_UNESCAPED_UNICODE)]);
    $params['sign']   = $this->getSign($params);
    $res              = Http::httpPost($this->domainUrl, $params);
    if ((isset($res['code']) && $res['code'] == 0) || !isset($res['code'])) {
      $this->setError($res);
      return false;
    }
    return $res;
  }
}
