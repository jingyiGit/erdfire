<?php

namespace Jy2dfire\Erdfire;

use Jy2dfire\Kernel\Http;

use Jy2dfire\Erdfire\Shop;
use Jy2dfire\Erdfire\Good;
use Jy2dfire\Erdfire\Order;
use Jy2dfire\Erdfire\Takeout;

class Erdfire
{
  use Shop;
  use Good;
  use Order;
  use Takeout;
  
  protected $domainUrl = 'http://gateway.2dfire.com';
  protected $config = [];
  protected $token = null;   // 店家授权到当前应用中的token
  protected $shop_id = null; // 店家在客如云里的商户编号
  protected $error = null;
  
  public function __construct($config = null)
  {
    $this->config = $config;
  }
  
  private function request($method, $params)
  {
    $params         = $this->handleGlobalParam($method, $params);
    $params['sign'] = $this->getSign($params);
    $res = Http::httpPost($this->domainUrl, $params);
    if ((isset($res['code']) && $res['code'] == 0) || !isset($res['code'])) {
      $this->setError($res);
      return false;
    }
    return $res;
  }
  
  private function post($url, $data)
  {
    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_POST, 1);
    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //不验证证书
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    
    $headers = [
      'Content-type:application/x-www-form-urlencoded;charset="utf-8"',
    ];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $output = curl_exec($ch);
    curl_close($ch);
    return json_decode($output, true) ?? $output;
  }
  
  /**
   * 获取签名
   *
   * @param $params
   * @return string
   */
  private function getSign($params)
  {
    $str = '';  //待签名字符串
    
    //先将参数以其参数名的字典序升序进行排序
    ksort($params);
    
    //遍历排序后的参数数组中的每一个key/value对
    foreach ($params as $k => $v) {
      $str .= $k . $v;
    }
    
    //将签名密钥拼接到签名字符串最后面
    $str = $this->config['secret'] . $str . $this->config['secret'];
    //加密并转成全大些字母
    return strtoupper(sha1($str));
  }
  
  /**
   * 处理全局参数
   *
   * @param $method
   * @param $params
   * @return array
   */
  private function handleGlobalParam($method, $params)
  {
    // 全局参数
    $globalParam = [
      'appKey'    => $this->config['key'],
      'method'    => $method,
      'v'         => '1',
      'locale'    => 'zh_CN',
      'format'    => 'JSON',
      'env'       => 'publish',                               // 日常(daily)，预发(pre)，正式(publish)
      'timestamp' => time() . rand(100, 999),                 // 组装时间戳，单位毫秒
    ];
    return array_merge($globalParam, $params);
  }
  
  private function handleReturn($res)
  {
    if ($res['code'] == 0 && $res['apiMessage'] == null) {
      $this->error = null;
      return $res['result'];
    }
    $this->setError($res);
    return false;
  }
  
  public function getError()
  {
    return $this->error;
  }
  
  private function setError($error)
  {
    $this->error = $error;
    return false;
  }
}
